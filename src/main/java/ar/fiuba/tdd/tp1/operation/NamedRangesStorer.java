package ar.fiuba.tdd.tp1.operation;

import java.util.HashMap;
import java.util.Map;

public class NamedRangesStorer {

    private static final NamedRangesStorer instance = new NamedRangesStorer();
    private static final HashMap<String, String> nameRange = new HashMap<>();

    public static NamedRangesStorer getInstance() {
        return instance;
    }

    public String replaceNamedRanges(String formula) {
        for (Map.Entry<String, String> entry : nameRange.entrySet()) {
            if (formula.contains(entry.getKey())) {
                formula = formula.replace(entry.getKey(), entry.getValue());
            }
        }
        return formula;
    }

    public void add(String name, String range) {
        nameRange.put(name, range);
    }
}
