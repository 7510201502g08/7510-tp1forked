package ar.fiuba.tdd.tp1.operation.newclases;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Today extends SimpleFunction {
    public Today(Cell cell) {
        super(cell);
    }

    @Override
    public void execute() {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(tz);
        String nowAsISO = df.format(new Date());
        setValue(nowAsISO);

        getTargetCell().setValue(nowAsISO);
    }

    @Override
    public String getValue() {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(tz);
        return df.format(new Date());
    }


}
