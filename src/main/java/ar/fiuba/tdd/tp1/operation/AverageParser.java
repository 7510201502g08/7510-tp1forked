package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

@SuppressWarnings("CPD-START")
public class AverageParser extends FunctionParser {

    public AverageParser(LinkedList<FunctionParser> functionParsers, LinkedList<BinaryOperationParser> operationParsers) {
        super(functionParsers, operationParsers);
    }

    @Override
    public String begin() {
        return "AVERAGE(";
    }

    @SuppressWarnings("CPD-END")
    @Override
    public Function createFunction(Cell targetCell) {
        return new Average(targetCell);
    }

}