package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/* Responsabilidades:
 * Devuelve un objeto Subtraction inicializado mediante el parseo de la fórmula recibida.
 */

public class SubstractionParser extends SeparatorParser {

    @Override
    public String getSymbol() {
        return "-";
    }

    @Override
    public BinaryOperation createBinaryOperation(Cell targetCell, String firstOperand, String secondOperand) {
        return new Subtraction(targetCell, firstOperand, secondOperand);
    }

}