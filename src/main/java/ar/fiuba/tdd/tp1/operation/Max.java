package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public class Max extends RangesFunction {

    public Max(Cell targetCell) {
        super(targetCell, false, true);
    }

    @Override
    public String processValuesRange(LinkedList<String> valuesRange) {
        return valuesRange.getLast();
    }

}