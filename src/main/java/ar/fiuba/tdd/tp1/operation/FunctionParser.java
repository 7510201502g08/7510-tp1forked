package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public abstract class FunctionParser {
    LinkedList<FunctionParser> functionParsers;
    LinkedList<BinaryOperationParser> operationParsers;

    public FunctionParser(LinkedList<FunctionParser> functionParsers,
                          LinkedList<BinaryOperationParser> operationParsers) {
        this.functionParsers = functionParsers;
        this.operationParsers = operationParsers;
    }

    public String end() {
        return ")";
    }

    public String parameterSeparator() {
        return ")";
    }

    public int maxParameters() {
        return 1;
    }

    public String getParameter(String formula) {
        int posEnd = formula.indexOf(end());
        int posNextParameterSeparator = formula.indexOf(parameterSeparator());
        if (posNextParameterSeparator < posEnd && posNextParameterSeparator != -1) {
            return formula.substring(0, posNextParameterSeparator);
        }
        return formula.substring(0, posEnd);
    }

    boolean isFunctionBegin(String formulaToParse) {
        String begin = begin();
        int beginLength = begin.length();
        try {
            if (formulaToParse.substring(0, beginLength).equals(begin)) {
                return true;
            }
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
        return false;
    }

    boolean isFunctionEnd(String formulaToParse) {
        String end = end();
        int endLength = end.length();
        try {
            if (formulaToParse.substring(0, endLength).equals(end)) {
                return true;
            }
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
        return false;
    }

    public LinkedList<FunctionParser> getFunctionParsers() {
        return functionParsers;
    }

    public LinkedList<BinaryOperationParser> getOperationParsers() {
        return operationParsers;
    }

    public abstract String begin();

    public abstract Function createFunction(Cell targetCell);
}
