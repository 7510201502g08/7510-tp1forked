package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public abstract class RangesFunction extends Function {

    LinkedList<Range> ranges = new LinkedList<>();
    boolean orderValues;

    public RangesFunction(Cell targetCell, boolean acceptsStringArguments, boolean orderValues) {
        super(targetCell, acceptsStringArguments);
        this.orderValues = orderValues;
    }

    public abstract String processValuesRange(LinkedList<String> valuesRange);

    public String handleResultsRanges(String resultPrecedingRange, String resultNewRange) {
        return resultNewRange;
    }

    @Override
    public void execute() {
        LinkedList<String> parameters = getParameters();
        for (String parameter : parameters) {
            Range range = new Range(getTargetCell().getSheet(), parameter, orderValues, this.acceptsStringArguments());
            ranges.add(range);
        }

        String result = "";
        for (Range range : ranges) {
            result = handleResultsRanges(result, processValuesRange(range.getValuesRange()));
        }

        setValue(result);
    }

}
