package ar.fiuba.tdd.tp1.operation.newclases;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public class Printf extends SimpleFunction {

    private Cell cell;

    public Printf(Cell targetCell) {
        super(targetCell);
        cell = targetCell;
    }

    @Override
    public void execute() {
        LinkedList<String> parameters = getParameters();
        String tempResult = parameters.get(0);
        String result = this.cell.getString(tempResult);

        for (int i = 1; i < parameters.size(); i++) {
            String replace = parameters.get(i);
            replace = " " + this.cell.getString(replace);
            String token = "$" + String.valueOf(i - 1);

            result = result.replace(token, replace);
        }
        setValue(result);
    }
}