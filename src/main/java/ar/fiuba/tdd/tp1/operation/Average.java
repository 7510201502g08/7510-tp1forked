package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public class Average extends RangesFunction {

    public Average(Cell targetCell) {
        super(targetCell, false, false);
    }

    @Override
    public String processValuesRange(LinkedList<String> valuesRange) {
        Double sum = 0.0;

        for (String value : valuesRange) {
            sum += Double.parseDouble(value);
        }

        Double result = sum / valuesRange.size();
        return Double.toString(result);
    }

}
