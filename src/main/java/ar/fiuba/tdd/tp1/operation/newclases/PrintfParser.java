package ar.fiuba.tdd.tp1.operation.newclases;

import ar.fiuba.tdd.tp1.operation.BinaryOperationParser;
import ar.fiuba.tdd.tp1.operation.Function;
import ar.fiuba.tdd.tp1.operation.FunctionParser;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public class PrintfParser extends FunctionParser {

    public PrintfParser(LinkedList<FunctionParser> functionParsers, LinkedList<BinaryOperationParser> operationParsers) {
        super(functionParsers, operationParsers);
    }

    @Override
    public String begin() {
        return "PRINTF(";
    }

    @Override
    public String parameterSeparator() {
        return ",";
    }

    @Override
    public Function createFunction(Cell targetCell) {
        return new Printf(targetCell);
    }
}  
