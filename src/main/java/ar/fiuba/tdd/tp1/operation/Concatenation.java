package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public class Concatenation extends RangesFunction {

    public Concatenation(Cell targetCell) {
        super(targetCell, true, false);
    }

    @Override
    public String processValuesRange(LinkedList<String> valuesRange) {

        String result = "";

        for (String value : valuesRange) {
            result = result.concat(value);
        }

        return result;
    }

    @Override
    public String handleResultsRanges(String resultPrecedingRange, String resultNewRange) {

        return (resultPrecedingRange.concat(resultNewRange));
    }


}
