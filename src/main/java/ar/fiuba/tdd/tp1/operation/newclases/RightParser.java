package ar.fiuba.tdd.tp1.operation.newclases;

import ar.fiuba.tdd.tp1.operation.BinaryOperationParser;
import ar.fiuba.tdd.tp1.operation.Function;
import ar.fiuba.tdd.tp1.operation.FunctionParser;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

@SuppressWarnings("CPD-START")
public class RightParser extends FunctionParser {

    public RightParser(LinkedList<FunctionParser> functionParsers, LinkedList<BinaryOperationParser> operationParsers) {
        super(functionParsers, operationParsers);
    }

    @Override
    public String begin() {
        return "RIGHT(";
    }

    @Override
    public String parameterSeparator() {
        return ",";
    }

    @SuppressWarnings("CPD-END")
    @Override
    public Function createFunction(Cell targetCell) {
        return new Right(targetCell);
    }
}  
