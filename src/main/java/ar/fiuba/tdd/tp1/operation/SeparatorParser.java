package ar.fiuba.tdd.tp1.operation;

public abstract class SeparatorParser extends BinaryOperationParser {

    @Override
    public boolean operatorIsSeparator() {
        return true;
    }
}
