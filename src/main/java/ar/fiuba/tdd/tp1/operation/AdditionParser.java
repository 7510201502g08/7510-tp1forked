package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/* Responsabilidades:
 * Devuelve un objeto Addition inicializado mediante el parseo de la fórmula recibida.
*/

public class AdditionParser extends SeparatorParser {

    @Override
    public String getSymbol() {
        return "+";
    }

    @Override
    public BinaryOperation createBinaryOperation(Cell targetCell, String firstOperand, String secondOperand) {
        return new Addition(targetCell, firstOperand, secondOperand);
    }

}