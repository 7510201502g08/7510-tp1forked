package ar.fiuba.tdd.tp1.operation.newclases;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

@SuppressWarnings("CPD-START")
public class Left extends SimpleFunction {

    private Cell cell;

    @Override
    public void execute() {
        LinkedList<String> parameters = getParameters();
        String original = parameters.get(0);
        original = this.cell.getString(original);
        String sizeString = parameters.get(1);
        sizeString = this.cell.getString(sizeString);

        int size = Integer.parseInt(sizeString);

        setValue(original.substring(0, size));
    }

    @SuppressWarnings("CPD-END")
    public Left(Cell targetCell) {
        super(targetCell);
        this.cell = targetCell;
    }
}