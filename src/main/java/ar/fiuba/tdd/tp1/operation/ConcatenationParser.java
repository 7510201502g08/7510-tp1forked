package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

@SuppressWarnings("CPD-START")
public class ConcatenationParser extends FunctionParser {

    public ConcatenationParser(LinkedList<FunctionParser> functionParsers,
                               LinkedList<BinaryOperationParser> operationParsers) {
        super(functionParsers, operationParsers);
    }

    @Override
    public String begin() {
        return "CONCAT(";
    }

    @Override
    public String parameterSeparator() {
        return ",";
    }

    @Override
    public int maxParameters() {
        return 2;
    }

    @SuppressWarnings("CPD-END")
    @Override
    public Function createFunction(Cell targetCell) {
        return new Concatenation(targetCell);
    }

}
