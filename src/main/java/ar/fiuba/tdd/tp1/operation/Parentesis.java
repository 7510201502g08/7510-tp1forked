package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public class Parentesis extends Function {

    LinkedList<FunctionParser> functionParsers;
    LinkedList<BinaryOperationParser> operationParsers;

    public Parentesis(Cell targetCell, LinkedList<FunctionParser> functionParsers, LinkedList<BinaryOperationParser> operationParsers) {
        super(targetCell, false);
        this.functionParsers = functionParsers;
        this.operationParsers = operationParsers;
    }

    @Override
    public void execute() {
        Formula formula = new Formula(getTargetCell(), getParameters().getFirst(), functionParsers, operationParsers, false);
        setValue(formula.getValue());

    }


}
