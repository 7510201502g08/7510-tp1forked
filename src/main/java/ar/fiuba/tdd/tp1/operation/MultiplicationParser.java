package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

public class MultiplicationParser extends BinaryOperationParser {

    @Override
    public String getSymbol() {
        return "*";
    }

    @Override
    public BinaryOperation createBinaryOperation(Cell targetCell, String firstOperand, String secondOperand) {
        return new Multiplication(targetCell, firstOperand, secondOperand);
    }

}
