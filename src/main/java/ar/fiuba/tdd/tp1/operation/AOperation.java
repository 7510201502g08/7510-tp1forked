package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/*  Responsabilidades:
 *  Establece los métodos base para cualquier operación. 
 *  Esta clase es necesaria para que OperationManager dependa de una abstracción y no de clases 
 * de operaciones concretas evitando violar el principio de Inversión de dependencia.
*/

public abstract class AOperation {

    private Cell targetCell;

    public Cell getTargetCell() {
        return this.targetCell;
    }

    public void setTargetCell(Cell cell) {
        this.targetCell = cell;
    }

    public abstract void execute();

}