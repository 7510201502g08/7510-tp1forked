package ar.fiuba.tdd.tp1.operation.newclases;

import ar.fiuba.tdd.tp1.operation.BinaryOperation;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;

public class Pow extends BinaryOperation {

    public Pow(Cell targetCell, String left, String right) {
        super(targetCell, left, right);
    }

    @Override
    public Double calculateResult() {
        return Math.pow(getTargetCell().getOperand(getLeft()), getTargetCell().getOperand(getRight()));
    }

}