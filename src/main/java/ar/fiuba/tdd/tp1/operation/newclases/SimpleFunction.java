package ar.fiuba.tdd.tp1.operation.newclases;

import ar.fiuba.tdd.tp1.operation.Function;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;

public abstract class SimpleFunction extends Function {

    public SimpleFunction(Cell targetCell) {
        super(targetCell, true);
    }

}
