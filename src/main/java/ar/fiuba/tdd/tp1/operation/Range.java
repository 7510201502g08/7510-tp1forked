package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;

import java.util.Collections;
import java.util.LinkedList;

public class Range {
    String[] rangeExtremes;
    Cell firstCellRange;
    Cell secondCellRange;
    LinkedList<String> valuesRange;
    boolean orderValues;
    boolean valuesAsString;
    private Sheet sheet;

    public Range(Sheet sheet, String rangeDescriptor, boolean orderValues, boolean valuesAsString) {
        rangeExtremes = rangeDescriptor.split(":");
        this.sheet = sheet;
        valuesRange = new LinkedList<>();
        this.orderValues = orderValues;
        this.valuesAsString = valuesAsString;

        validateFormat();

        if (!hasStringParameter()) {
            if (invalidNumberOfParameters()) {
                throw new IllegalArgumentException();
            }
            firstCellRange = sheet.getCell(rangeExtremes[0]);
            if (oneCellOnly()) {
                secondCellRange = firstCellRange;
            } else {
                secondCellRange = sheet.getCell(rangeExtremes[1]);
            }

            validateRangeExtremes();
        }

        setValuesRange();
    }

    public void validateRangeExtremes() {
        validateSameSheet();
        validateSameRowOrColumn();
        validateOrder();

    }

    public void validateSameSheet() {
        if (!extremesAreInSameSheet()) {
            throw new IllegalArgumentException();
        }
    }

    public void validateSameRowOrColumn() {
        if (!extremesAreInSameRow() && !extremesAreInSameColumn()) {
            throw new IllegalArgumentException();
        }
    }

    public void validateOrder() {
        if (extremesAreInSameRow() && firstExtremeColumnIsMajor()) {
            throw new IllegalArgumentException();
        }
        if (extremesAreInSameColumn() && firstExtremeRowIsMajor()) {
            throw new IllegalArgumentException();
        }
    }

    public void validateFormat() {
        if (invalidFormat()) {
            throw new IllegalArgumentException();
        }
    }

    public boolean invalidNumberOfParameters() {
        return (rangeExtremes.length > 2);
    }

    public boolean oneCellOnly() {
        return (rangeExtremes.length == 1);
    }

    public boolean extremesAreInSameSheet() {
        return (firstCellRange.getSheet().getName().equals(secondCellRange.getSheet().getName()));
    }

    public boolean extremesAreInSameRow() {
        return (firstCellRange.getRow() == secondCellRange.getRow());
    }

    public boolean extremesAreInSameColumn() {
        return (firstCellRange.getColumn() == secondCellRange.getColumn());
    }

    public boolean firstExtremeRowIsMajor() {
        return (firstCellRange.getRow() > secondCellRange.getRow());
    }

    public boolean firstExtremeColumnIsMajor() {
        return (firstCellRange.getColumn() > secondCellRange.getColumn());
    }

    public boolean hasStringParameter() {
        try {
            sheet.getCell(rangeExtremes[0]);
            if (!oneCellOnly()) {
                sheet.getCell(rangeExtremes[1]);
            }
        } catch (Exception e) {
            return true;
        }
        return false;
    }

    public boolean invalidFormat() {
        return (hasStringParameter() && rangeExtremes.length > 1);
    }

    public void setValuesRange() {

        if (!hasStringParameter()) {
            constructRange();
        } else {
            valuesRange.add(rangeExtremes[0]);
        }

        if (orderValues) {
            Collections.sort(valuesRange);
        }
    }

    public void constructRange() {
        if (extremesAreInSameColumn()) {
            for (int i = firstCellRange.getRow(); i <= secondCellRange.getRow(); i++) {
                valuesRange.add(getCellValue(i, firstCellRange.getColumn()));
            }
        } else {
            for (int i = firstCellRange.getColumn(); i <= secondCellRange.getColumn(); i++) {
                valuesRange.add(getCellValue(firstCellRange.getRow(), i));
            }
        }
    }

    public String getCellValue(int row, int column) {
        String value = sheet.getCell(row, column).getValue();
        if (valuesAsString) {
            return truncateDecimals(value);
        }
        return value;
    }

    public String truncateDecimals(String value) {
        try {
            Double.parseDouble(value);
            return value.split("\\.")[0];
        } catch (Exception e) {
            return value;
        }
    }

    public LinkedList<String> getValuesRange() {
        return valuesRange;
    }
}
