package ar.fiuba.tdd.tp1.operation.newclases;

import ar.fiuba.tdd.tp1.operation.BinaryOperation;
import ar.fiuba.tdd.tp1.operation.SeparatorParser;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;

public class PowParser extends SeparatorParser {

    @Override
    public String getSymbol() {
        return "^";
    }

    @Override
    public BinaryOperation createBinaryOperation(Cell targetCell, String firstOperand, String secondOperand) {
        return new Pow(targetCell, firstOperand, secondOperand);
    }

}