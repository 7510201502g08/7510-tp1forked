package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

public class Multiplication extends BinaryOperation {

    public Multiplication(Cell targetCell, String left, String right) {
        super(targetCell, left, right);
    }

    @Override
    public Double calculateResult() {
        return (getTargetCell().getOperand(getLeft()) * getTargetCell().getOperand(getRight()));
    }

}
