package ar.fiuba.tdd.tp1.formatter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Map;

public class NumberFormatter extends Formatter {

    public String notNumber = "NOT NUMBER";
    private int decimals;

    public NumberFormatter(int decimals) {
        this.decimals = decimals;
        super.setName("Number");
    }

    public void setDecimal(int decimals) {
        this.decimals = decimals;
    }

    @Override
    public String getFormattedValue(String value) {
        StringBuilder sb = new StringBuilder();
        if (decimals > 0) {
            sb.append("#.");
            for (int i = 0; i < decimals; i++) {
                sb.append("0");
            }
        }
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator('.');
        DecimalFormat df = new DecimalFormat(sb.toString(), otherSymbols);
        try {
            return df.format(Double.parseDouble(value));
        } catch (Exception ex) {
            return notNumber;
        }
    }

    @Override
    public boolean isValid(String value) {
        try {
            new Double(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public int getDecimals() {
        return this.decimals;
    }

    @Override
    public void modifyParameters(Map<String, String> parameters) {
        parameters.put("Number.Decimal", String.valueOf(decimals));
    }

}
