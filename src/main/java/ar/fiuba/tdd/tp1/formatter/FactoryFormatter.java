package ar.fiuba.tdd.tp1.formatter;

import java.util.Map;

public class FactoryFormatter {

    public static Formatter createFormat(Map<String, Object> parametters) {
        String formatName = (String) parametters.get("name");
        switch (formatName) {
            case "Date": {
                String value = (String) parametters.get("Date.Format");
                return new DateFormatter(value);
            }
            case "Number": {
                String value = (String) parametters.get("Number.Decimal");
                return new NumberFormatter(Integer.parseInt(value));
            }
            case "Money": {
                String value = (String) parametters.get("Number.Decimal");
                String symbol = (String) parametters.get("Money.Symbol");
                return new MoneyFormatter(Integer.parseInt(value), symbol);
            }
            default:
                return new StringFormatter();
        }
    }
}
