package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.operation.NamedRangesStorer;
import ar.fiuba.tdd.tp1.spreadsheet.Book;

import java.io.IOException;

public class NamedRangeCreator extends CommandMenu {

    @Override
    public String getDescription() {
        return "Crear un named range";
    }

    @Override
    public void execute(Book book) throws IOException {
        //System.out.println("Ingrese nombre hoja:");
        //final String sheet = br.readLine();
        System.out.println("Ingrese rango de forma INI:FIN :");
        String range = br.readLine();
        System.out.println("Ingrese nombre para el rango:");
        String name = br.readLine();

        NamedRangesStorer storer = NamedRangesStorer.getInstance();
        storer.add(name, range);
    }
}
