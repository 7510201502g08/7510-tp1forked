package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public abstract class Importer {

    static int SIZE_ROWS = 100;
    static int SIZE_COLUMNS = 100;

    public abstract Book importFile(String path) throws Exception;

    public Book modifyBook(Book book, Collection<Map<String, Object>> sheetsCellsList) {

        Collection<Sheet> sheets = new LinkedList<>();
        Sheet sheet = null;
        String sheetNameAnterior = "";
        for (Map<String, Object> sheetCell : sheetsCellsList) {
            String sheetName = (String) sheetCell.get("sheet");
            if (!sheetNameAnterior.equals(sheetName)) {
                sheet = new Sheet(book, sheetName, SIZE_ROWS, SIZE_COLUMNS);
                sheets.add(sheet);
                sheetNameAnterior = sheetName;
            }
            this.modifySheet(sheet, sheetCell);
        }
        book.setSheets(sheets);
        return book;
    }

    public abstract void modifyCellWithFormatter(Cell cell, Map<String, Object> sheetCell);

    private void modifySheet(Sheet sheet, Map<String, Object> sheetCell) {
        Cell cell = sheet.getCell((String) sheetCell.get("id"));

        this.modifyCellWithFormatter(cell, sheetCell);

        cell.setValue((String) sheetCell.get("value"));
    }

    private Map<String, Object> convertToMap(JSONObject parametersJson) {
        Map<String, Object> map = new HashMap<>();
        Iterator keys = parametersJson.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();
            String value = "";
            try {
                value = parametersJson.getString(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            map.put(key, value);

        }
        return map;
    }
}