package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.spreadsheet.Book;

import java.io.File;
import java.io.IOException;

public abstract class BookIO extends CommandMenu {

    static final String DIR_NAME = "/archivos/";
    private String description;
    private Exporter exporter;
    private Importer importer;

    public BookIO(String description, Exporter exporter, Importer importer) {
        this.description = description;
        this.exporter = exporter;
        this.importer = importer;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void execute(Book book) throws IOException {
        try {
            if (exporter != null) {
                executeExport(book);
            } else if (importer != null) {
                book.importBook(executeImport());
            }
        } catch (Exception e) {
            System.out.println("Error en la exportacion");
        }
    }

    private void executeExport(Book book) throws Exception {
        System.out.println("Ingrese nombre del archivo (ubicado en la carpeta archivos): ");
        String fileName = br.readLine();
        System.out.println("Ingrese nombre del book: ");
        String bookName = br.readLine();
        exporter.exportAFile(book, fileName, bookName);
    }

    private Book executeImport() throws Exception {
        boolean existFile = false;
        String path = null;
        while (!existFile) {
            System.out.println("Ingrese nombre del archivo (ubicado en la carpeta archivos): ");
            String fileName = br.readLine();
            path = System.getProperty("user.dir") + DIR_NAME + fileName;
            existFile = istFile(path);
        }
        return importer.importFile(path);
    }

    private boolean istFile(String path) {
        boolean isFile = false;
        try {
            File file = new File(path);
            if (file.exists() && !file.isDirectory()) {
                isFile = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return isFile;
    }
}
