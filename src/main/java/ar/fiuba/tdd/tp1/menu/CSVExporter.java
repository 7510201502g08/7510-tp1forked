package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import com.csvreader.CsvWriter;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

public class CSVExporter extends Exporter {

    static final String DELIMITER = ",";
    static final String COLUMNSREGISTER = "id,value,sheet";
    static final String DIR_NAME = "/archivos/";

    public CSVExporter() {

    }

    public void exportAFile(Book book, String bookName, String path) throws Exception {

        final String actuallyDir = System.getProperty("user.dir");
        path = actuallyDir + DIR_NAME + path;

        CsvWriter writer = new CsvWriter(path);
        String[] registerName = COLUMNSREGISTER.split(DELIMITER);
        writer.writeRecord(registerName);

        Collection<Map<String, Object>> cellsList = new LinkedList<>();

        for (Sheet sheet : book.getSheets()) {
            writeCellsInCellsList(sheet, cellsList);
        }

        this.setDataInACsvWriter(writer, cellsList);
        writer.close();
    }

    @Override
    public void modifyCellMap(Cell cell, Map<String, Object> cellMap) {

    }

    private void setDataInACsvWriter(CsvWriter writer, Collection<Map<String, Object>> cellsList) {

        for (Map<String, Object> rowOfDataMap : cellsList) {
            String id = (String) rowOfDataMap.get("id");
            String value = String.valueOf(rowOfDataMap.get("value"));
            String sheet = (String) rowOfDataMap.get("sheet");
            String[] rowOfData = new String[]{id, value, sheet};
            try {
                writer.writeRecord(rowOfData);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
