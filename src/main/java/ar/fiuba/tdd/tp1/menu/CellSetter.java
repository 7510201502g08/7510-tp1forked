package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.formatter.*;
import ar.fiuba.tdd.tp1.spreadsheet.Book;

import java.io.IOException;

public class CellSetter extends CommandMenu {

    @Override
    public String getDescription() {
        return "Ingresar valor a celda";
    }

    @Override
    public void execute(Book book) throws IOException {
        System.out.println("Ingrese nombre hoja:");
        final String sheet = br.readLine();
        System.out.println("Ingrese nombre celda:");
        String cell = br.readLine();
        System.out.println("Ingrese formato celda (texto, fecha, numero, moneda) o Enter para no cambiarlo:");
        String formatter = br.readLine();
        System.out.println("Ingrese valor celda:");
        String cellValue = br.readLine();
        if (sheet != null && cell != null && cellValue != null) {
            book.getSheet(sheet).getCell(cell).setFormatter(getFormatterByName(formatter));
            if (cellValue.contains("=")) {
                book.getSheet(sheet).getCell(cell).setFormula(cellValue.substring(1));
            } else {
                book.getSheet(sheet).getCell(cell).setValue(cellValue);
            }
        }
    }

    private Formatter getFormatterByName(String formatter) {
        switch (formatter) {
            case "fecha":
                return new DateFormatter("dd-MM-yyyy");
            case "numero":
                return new NumberFormatter(2);
            case "moneda":
                return new MoneyFormatter(2, "ARS");
            default:
                return new StringFormatter();
        }
    }

}
