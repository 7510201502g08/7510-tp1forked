package ar.fiuba.tdd.tp1.menu;

public class CSVBookImporter extends BookIO {

    public CSVBookImporter() {
        super("Importar desde CSV", null, new CsvImporter());
    }

}
