package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.spreadsheet.Book;

import java.io.IOException;

public class SheetCreator extends CommandMenu {

    @Override
    public void execute(Book book) throws IOException {
        System.out.println("Ingrese nombre:");
        String name = br.readLine();
        book.createNewSheet(name, 100, 100);
    }

    @Override
    public String getDescription() {
        return "Crear nueva hoja ";
    }

}
