package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class Exporter {
    public abstract void exportAFile(Book book, String fileName, String bookName) throws Exception;

    public void writeCellsInCellsList(Sheet sheet, Collection<Map<String, Object>> cellsList) {
        int rows = sheet.getRows();
        int columns = sheet.getColumns();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                Cell cell = sheet.getCell(i, j);
                if (cell.inUse()) {
                    Map<String, Object> cellMap = new HashMap<>();
                    modifyCellMap(cell, cellMap);

                    String formula = cell.getFormula();
                    // CHEQUEAR QUE NO LLEGUE UNA FORMULA EN NULL
                    if (formula == null) {
                        formula = "";
                    }
                    cellMap.put("value", cell.getValue());
                    cellMap.put("formula", formula);
                    cellMap.put("sheet", sheet.getName());
                    cellMap.put("id", cell.getIdentifier());

                    cellsList.add(cellMap);
                }
            }
        }
    }

    public abstract void modifyCellMap(Cell cell, Map<String, Object> cellMap);

}