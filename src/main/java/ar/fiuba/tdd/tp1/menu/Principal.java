package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.spreadsheet.Book;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class Principal {

    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in, Charset.forName("UTF-8")));
    private static Map<Integer, CommandMenu> commands = new HashMap<>();

    public static void main(String[] args) throws Exception {

        Book book = new Book();

        initializeCommands();

        while (true) {
            renderMenu();
            String option = br.readLine();
            int commandNumber = 0;
            try {
                commandNumber = Integer.parseInt(option);
            } catch (Exception ex) {
                System.out.println("Ingrese opción válida");
            }
            CommandMenu command = commands.get(commandNumber);
            if (command != null) {
                command.execute(book);
            } else {
                System.out.println("Ingrese opción válida");
            }
        }
    }

    private static void initializeCommands() {
        int idx = 0;
        commands.put(++idx, new SheetCreator());
        commands.put(++idx, new CellSetter());
        commands.put(++idx, new BookPrinter());
        commands.put(++idx, new NamedRangeCreator());
        commands.put(++idx, new CSVBookExporter());
        commands.put(++idx, new JsonBookExporter());
        commands.put(++idx, new JsonBookImporter());
        commands.put(++idx, new HelperCommand());
    }

    private static void renderMenu() {
        System.out.println("Seleccione opción:");
        for (Map.Entry<Integer, CommandMenu> entry : commands.entrySet()) {
            System.out.println(entry.getKey() + ")" + entry.getValue().getDescription());
        }
    }

}