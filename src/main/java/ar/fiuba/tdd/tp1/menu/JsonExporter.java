package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

public class JsonExporter extends Exporter {

    private JSONObject jsonObject;

    public JsonExporter() {

    }

    @Override
    public void exportAFile(Book book, String fileName, String bookName) throws JSONException, IOException {

        this.jsonObject = new JSONObject();

        Collection<Map<String, Object>> cellList = new LinkedList<>();

        for (Sheet sheet : book.getSheets()) {
            this.writeCellsInCellsList(sheet, cellList);
        }

        jsonObject.put("name", bookName);

        JSONArray cellsJson = new JSONArray();
        this.modifyCellsJson(cellsJson, cellList);

        jsonObject.put("cells", cellsJson);

        String text = convertToPlaneText(jsonObject);
        if (text != null) {
            Helper writer = new Helper();
            writer.fileCreate(text, fileName);
        }
    }

    private void modifyCellsJson(JSONArray cellsJson, Collection<Map<String, Object>> cellList) throws JSONException {

        for (Map<String, Object> cell : cellList) {
            JSONObject cellJson = new JSONObject();

            JSONObject formatterJson = new JSONObject();
            //JSONArray formattersJson = new JSONArray();
            Map<String, String> parametersMap = (Map<String, String>) cell.get("formatter");

            for (Map.Entry<String, String> entry : parametersMap.entrySet()) {
                String key = entry.getKey();
                formatterJson.put(key, entry.getValue());
            }

            this.modifyCellJson(cellJson, cell, formatterJson);

            //formattersJson.put(formatterJson);
            cellJson.put("formatter", formatterJson);
            cellsJson.put(cellJson);
        }
    }

    public void modifyCellJson(JSONObject cellJson, Map<String, Object> cell, JSONObject formatterJson) throws JSONException {

        cellJson.put("sheet", cell.get("sheet"));
        cellJson.put("id", cell.get("id"));
        cellJson.put("value", cell.get("value"));
        cellJson.put("formula", cell.get("formula"));
        cellJson.put("type", formatterJson.getString("name"));
        formatterJson.remove("name");
    }

    @Override
    public void modifyCellMap(Cell cell, Map<String, Object> cellMap) {

        Map<String, String> parametersMap = cell.getFormatter().getParamters();
        cellMap.put("formatter", parametersMap);
    }

    private String convertToPlaneText(JSONObject jsonObject) {

        String text = null;
        try {
            text = jsonObject.toString();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return text;
    }

}