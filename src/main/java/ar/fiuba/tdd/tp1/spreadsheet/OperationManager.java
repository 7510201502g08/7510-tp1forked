package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.operation.ReversibleOperation;

import java.util.Stack;

/* Responsabilidades:
 * Encargada de las tareas de rehacer y deshacer operaciones en un libro.
 */
public class OperationManager {

    private Stack<ReversibleOperation> undoOperations;
    private Stack<ReversibleOperation> redoOperations;

    public OperationManager() {
        undoOperations = new Stack<>();
        redoOperations = new Stack<>();
    }

    public void addNewOperation(ReversibleOperation operation) {
        undoOperations.push(operation);
        redoOperations.clear();
    }

    public void undo() {
        if (!undoOperations.empty()) {
            ReversibleOperation operationToUndo = undoOperations.pop();
            redoOperations.push(operationToUndo);
            operationToUndo.unexecute();
        }
    }

    public void redo() {
        if (!redoOperations.empty()) {
            ReversibleOperation operationToRedo = redoOperations.pop();
            undoOperations.push(operationToRedo);
            operationToRedo.execute();
        }
    }

}