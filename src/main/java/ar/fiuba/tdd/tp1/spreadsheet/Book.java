package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.operation.*;
import ar.fiuba.tdd.tp1.operation.newclases.*;

import java.util.Collection;
import java.util.LinkedList;

/* Responsabilidades: 
 * Contiene las hojas del libro y un OperationManager general de todo el libro.
 */

public class Book {

    protected Collection<Sheet> sheets;
    LinkedList<FunctionParser> functionParsers;
    LinkedList<BinaryOperationParser> operationParsers;
    int maxLengthColName = 0;
    private String name;
    private OperationManager operationManager;

    public Book() {
        this.initialize();
    }

    public Book(String bookName) {
        this.initialize();
        this.name = bookName;
    }

    public Book(LinkedList<FunctionParser> functionParsers, LinkedList<BinaryOperationParser> operationParsers) {
        sheets = new LinkedList<>();
        operationManager = new OperationManager();
        this.functionParsers = functionParsers;
        this.operationParsers = operationParsers;
    }

    public Collection<Sheet> getSheets() {
        return sheets;
    }

    public void setSheets(Collection<Sheet> sheets) {
        this.sheets = sheets;
    }

    private void initialize() {
        sheets = new LinkedList<>();
        operationManager = new OperationManager();
        functionParsers = new LinkedList<>();
        operationParsers = new LinkedList<>();

        initializeDefaultFunctionParsers();
        initializeDefaultOperationParsers();
    }

    public void importBook(Book book) {
        this.setSheets(book.getAllSheets());
    }

    void initializeDefaultFunctionParsers() {
        functionParsers.add(new ParenthesisParser(functionParsers, operationParsers));
        functionParsers.add(new ConcatenationParser(functionParsers, operationParsers));
        functionParsers.add(new AverageParser(functionParsers, operationParsers));
        functionParsers.add(new MinParser(functionParsers, operationParsers));
        functionParsers.add(new MaxParser(functionParsers, operationParsers));
        functionParsers.add(new LeftParser(functionParsers, operationParsers));
        functionParsers.add(new RightParser(functionParsers, operationParsers));
        functionParsers.add(new TodayParser(functionParsers, operationParsers));
        functionParsers.add(new PrintfParser(functionParsers, operationParsers));
    }

    void initializeDefaultOperationParsers() {
        operationParsers.add(new AdditionParser());
        operationParsers.add(new SubstractionParser());
        operationParsers.add(new MultiplicationParser());
        operationParsers.add(new PowParser());
    }

    public Sheet createNewSheet(String name, int numberOfRows, int numberOfColumns) {
        SheetCreation sheetCreation = new SheetCreation(this, name, numberOfRows, numberOfColumns);
        addOperation(sheetCreation);
        sheetCreation.execute();
        return getSheet(name);
    }

    public void deleteASheet(Sheet sheet) {
        sheets.remove(sheet);
    }

    public LinkedList<FunctionParser> getFunctionParsers() {
        return functionParsers;
    }

    public LinkedList<BinaryOperationParser> getOperationParsers() {
        return operationParsers;
    }

    public void addOperation(ReversibleOperation operation) {
        operationManager.addNewOperation(operation);
    }

    public void undo() {
        this.operationManager.undo();
    }

    public void redo() {
        this.operationManager.redo();
    }

    public Sheet getSheet(String sheetName) {
        for (Sheet sheet : sheets) {
            if (sheet.getName().equals(sheetName)) {
                return sheet;
            }
        }
        throw new IllegalArgumentException();
    }

    public Collection<Sheet> getAllSheets() {
        return sheets;
    }

    public Cell getCell(String identifier) {
        String[] sheetNameAndCellId = identifier.split("!");
        if (sheetNameAndCellId.length > 2) {
            throw new IllegalArgumentException();
        }
        Sheet sheet = getSheet(sheetNameAndCellId[0]);
        return sheet.getCell(sheetNameAndCellId[1]);
    }

    public LinkedList<String> getSheetsNames() {
        LinkedList<String> sheetsNames = new LinkedList<>();
        for (Sheet sheet : sheets) {
            sheetsNames.add(sheet.getName());
        }
        return sheetsNames;
    }

    public boolean isValidCellName(String cellName) {
        String[] sheetNameAndCellId = cellName.split("!");
        return sheetNameAndCellId.length <= 2 && (getSheet(sheetNameAndCellId[0]).isValidCellName(sheetNameAndCellId[1]));
    }

    public boolean isValidRowNumber(int rowNumber) {
        for (Sheet sheet : sheets) {
            if (sheet.isValidRowNumber(rowNumber)) {
                return true;
            }
        }
        return false;
    }

    public boolean isValidColumnName(String colName) {
        for (Sheet sheet : sheets) {
            if (sheet.isValidColumnName(colName)) {
                return true;
            }
        }
        return false;
    }

    public boolean isValidColumnName(String colName, String sheetName) {
        try {
            Sheet sheet = getSheet(sheetName);
            return (sheet.isValidColumnName(colName));
        } catch (Exception e) {
            return false;
        }
    }

    public int getMaxLengthColName() {
        return maxLengthColName;
    }

    public void setMaxLengthColName(int maxLengthColName) {
        this.maxLengthColName = maxLengthColName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}