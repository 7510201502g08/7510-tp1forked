package ar.fiuba.tdd.tp1.spreadsheet;

import java.util.LinkedList;

/* Responsabilidades:
 * Contiene las celdas de la hoja, puede invocar a recalcular todas las celdas y devuelve celdas.
 */
public class Sheet {
    static final int ABC_LENGTH = 26;
    static final String IDENTIFIER_OF_OTHER_BOOk = "!";
    static final char FIRST_OF_ABC = 'A';
    static final int DIFERENCE_OF_EXT_AND_INT_NUMBER = 1;
    static final int NEUTRO_NUMBER = 0;
    private Book book;
    private String name;
    private Cell[][] cells;
    private int rows;
    private int columns;
    private int maxLengthColName;
    private LinkedList<String> columnNames = new LinkedList<>();

    public Sheet(Book book, String name, int rows, int columns) {
        this.book = book;
        this.name = name;
        cells = new Cell[rows][columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                String identifier = convertToIdentifier(i, j);
                cells[i][j] = new Cell(this, identifier, i, j);
                if (i == rows - 1) {
                    String colName = identifier.replaceAll("[0-9]", "");
                    columnNames.add(colName);
                    if (j == columns - 1) {
                        maxLengthColName = colName.length();
                    }
                }
            }
        }
        setColumns(columns);
        setRows(rows);
    }

    private int getExternalNumber(int internalNumber) {
        return internalNumber + DIFERENCE_OF_EXT_AND_INT_NUMBER;
    }

    private String convertToIdentifier(int internalRowNumber, int internalColumnNumber) {
        return columnNumberToName(internalColumnNumber) + (getExternalNumber(internalRowNumber));
    }

    private String columnNumberToName(int internalColumnNumber) {
        int externalColumnNumber = getExternalNumber(internalColumnNumber);
        StringBuilder sb = new StringBuilder();
        while (externalColumnNumber-- > NEUTRO_NUMBER) {
            sb.append((char) (FIRST_OF_ABC + (externalColumnNumber % ABC_LENGTH)));
            externalColumnNumber /= ABC_LENGTH;
        }
        return sb.reverse().toString();
    }

    private boolean isValidPosition(int row, int column) {
        return (row < rows) && (column < columns);
    }

    private Cell getCellCorrect(String identifier) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (cells[i][j].getIdentifier().equals(identifier)) {
                    return cells[i][j];
                }
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Cell getCell(int row, int column) {
        if (isValidPosition(row, column)) {
            return cells[row][column];
        } else {
            throw new IllegalArgumentException();
        }
    }

    public Cell getCell(String identifier) {
        if (identifier.contains(IDENTIFIER_OF_OTHER_BOOk)) {
            return book.getCell(identifier);
        }
        Cell cell = getCellCorrect(identifier);
        if (cell != null) {
            return cell;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public Book getBook() {
        return this.book;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public boolean isValidRowNumber(int rowNumber) {
        return (rowNumber <= rows && rowNumber > 0);
    }

    public boolean isValidColumnName(String columnName) {
        return (columnNames.contains(columnName));
    }

    public boolean isValidCellName(String cellName) {
        if (cellName.contains(IDENTIFIER_OF_OTHER_BOOk)) {
            return book.isValidCellName(cellName);
        }
        String colName = cellName.replaceAll("[0-9]", "");
        int rowNumber = Integer.parseInt(cellName.replaceAll("\\D+", ""));
        return (isValidRowNumber(rowNumber) && isValidColumnName(colName));
    }

    public int getMaxLengthColName() {
        return maxLengthColName;
    }
}