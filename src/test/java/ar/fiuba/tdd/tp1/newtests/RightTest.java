package ar.fiuba.tdd.tp1.newtests;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RightTest {

    private Book book;
    private Sheet sheet;

    @Before
    public void setUp() {
        book = new Book();
        sheet = book.createNewSheet("hoja1", 100, 100);

        Cell cell1 = sheet.getCell("A1");
        cell1.setValue("HOLA");

        Cell cell2 = sheet.getCell("A2");
        cell2.setValue("3");
    }

    @After
    public void tearDown() {
        sheet = null;
        book = null;
    }

    @Test
    public void basicRightTest1() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("RIGHT(A1,A2)");

        assertEquals("OLA", cell.getValue());
    }

    @Test
    public void basicRightTest2() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("RIGHT(HOLA,A2)");

        assertEquals("OLA", cell.getValue());
    }

    @Test
    public void basicRightTest3() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("RIGHT(A1,3)");

        assertEquals("OLA", cell.getValue());
    }

    @Test
    public void basicRightTest4() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("RIGHT(HOLA,3)");

        assertEquals("OLA", cell.getValue());
    }

    @Test
    public void basicRightTest5() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("RIGHT(HOLA,20)");

        assertEquals("HOLA", cell.getValue());
    }
}
