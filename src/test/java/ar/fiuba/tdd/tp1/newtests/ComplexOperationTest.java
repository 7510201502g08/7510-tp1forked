package ar.fiuba.tdd.tp1.newtests;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ComplexOperationTest {

    private Book book;
    private Sheet sheet;

    @Before
    public void setUp() {
        book = new Book();
        sheet = book.createNewSheet("hoja1", 100, 100);

        Cell cell1 = sheet.getCell("A1");
        cell1.setValue("5");

        Cell cell2 = sheet.getCell("A2");
        cell2.setValue("3");
    }

    @After
    public void tearDown() {
        sheet = null;
        book = null;
    }

    @Test
    public void complexPow1() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("A1^A2+3");

        assertEquals(String.valueOf(Math.pow(5, 3) + 3), cell.getValue());
    }

    @Ignore
    @Test
    public void complexPow2() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("3+A1^A2");

        assertEquals(String.valueOf(Math.pow(5, 3) + 3), cell.getValue());
    }

    @Test
    public void complexPowParenthesis1() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("(A1^A2)+3");

        assertEquals(String.valueOf(Math.pow(5, 3) + 3), cell.getValue());
    }

    @Test
    public void complexPowParenthesis2() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("3+(A1^A2)");

        assertEquals(String.valueOf(Math.pow(5, 3) + 3), cell.getValue());
    }

    @Test
    public void complexPowParenthesis3() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("A1^(A2+3)");

        assertEquals(String.valueOf(Math.pow(5, 6)), cell.getValue());
    }

    @Test
    public void complexPowParenthesis4() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("(3+A1)^A2");

        assertEquals(String.valueOf(Math.pow(8, 3)), cell.getValue());
    }
}
