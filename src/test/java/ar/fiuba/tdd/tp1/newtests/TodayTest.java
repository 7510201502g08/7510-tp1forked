package ar.fiuba.tdd.tp1.newtests;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;

public class TodayTest {

    private Book book;
    private Sheet sheet;

    @Before
    public void setUp() {
        book = new Book();
        sheet = book.createNewSheet("hoja1", 100, 100);
    }

    @After
    public void tearDown() {
        sheet = null;
        book = null;
    }

    @Test
    public void basicTodayTest() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("TODAY()");

        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(tz);

        assertEquals(df.format(new Date()), cell.getValue());
    }
}
