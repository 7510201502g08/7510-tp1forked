package ar.fiuba.tdd.tp1.newtests;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LeftTest {

    private Book book;
    private Sheet sheet;

    @Before
    public void setUp() {
        book = new Book();
        sheet = book.createNewSheet("hoja1", 100, 100);

        Cell cell1 = sheet.getCell("A1");
        cell1.setValue("HOLA");

        Cell cell2 = sheet.getCell("A2");
        cell2.setValue("3");
    }

    @After
    public void tearDown() {
        sheet = null;
        book = null;
    }

    @Test
    public void basicLeftTest1() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("LEFT(A1,A2)");

        assertEquals("HOL", cell.getValue());
    }

    @Test
    public void basicLeftTest2() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("LEFT(HOLA,A2)");

        assertEquals("HOL", cell.getValue());
    }

    @Test
    public void basicLeftTest3() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("LEFT(A1,3)");

        assertEquals("HOL", cell.getValue());
    }

    @Test
    public void basicLeftTest4() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("LEFT(HOLA,3)");

        assertEquals("HOL", cell.getValue());
    }
}
