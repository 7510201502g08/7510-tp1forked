package ar.fiuba.tdd.tp1.newtests;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PowTest {

    private Book book;
    private Sheet sheet;

    @Before
    public void setUp() {
        book = new Book();
        sheet = book.createNewSheet("hoja1", 100, 100);

        Cell cell1 = sheet.getCell("A1");
        cell1.setValue("5");

        Cell cell2 = sheet.getCell("A2");
        cell2.setValue("3");
    }

    @After
    public void tearDown() {
        sheet = null;
        book = null;
    }

    @Test
    public void basicPow1() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("A1^A2");

        assertEquals(String.valueOf(Math.pow(5, 3)), cell.getValue());
    }

    @Test
    public void basicPow2() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("A2^A1");

        assertEquals(String.valueOf(Math.pow(3, 5)), cell.getValue());
    }

    @Test
    public void basicPow3() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("A1^A1");

        assertEquals(String.valueOf(Math.pow(5, 5)), cell.getValue());
    }
}
