package ar.fiuba.tdd.tp1.newtests;


import ar.fiuba.tdd.tp1.operation.NamedRangesStorer;
import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NamedRangesTest {

    private Book book;
    private Sheet sheet;

    @Before
    public void setUp() {
        book = new Book();
        sheet = book.createNewSheet("hoja1", 100, 100);

        Cell cell1 = sheet.getCell("A1");
        cell1.setValue("5");

        Cell cell2 = sheet.getCell("A2");
        cell2.setValue("3");

        Cell cell3 = sheet.getCell("A3");
        cell3.setValue("1");

        Cell cell4 = sheet.getCell("A4");
        cell4.setValue("7");

        NamedRangesStorer storer = NamedRangesStorer.getInstance();

        storer.add("range1", "A1:A2");
        storer.add("range2", "A1:A4");
    }

    @After
    public void tearDown() {
        sheet = null;
        book = null;
    }

    @Test
    public void simepleOperationUsingNamedRange1Test() {
        Cell cell1 = sheet.getCell("B1");
        cell1.setFormula("MIN(A1:A2)");

        Cell cell2 = sheet.getCell("B2");
        cell2.setFormula("MIN(range1)");

        assertEquals(cell1.getValue(), cell2.getValue());
    }

    @Test
    public void simepleOperationUsingNamedRange2Test() {
        Cell cell1 = sheet.getCell("B1");
        cell1.setFormula("AVERAGE(A1:A4)");

        Cell cell2 = sheet.getCell("B2");
        cell2.setFormula("AVERAGE(range2)");

        assertEquals(cell1.getValue(), cell2.getValue());
    }

    @Test
    public void complexOperationUsingNamedRange1Test() {

        Cell cell1 = sheet.getCell("B3");
        cell1.setFormula("MAX(range1) + A2");

        assertEquals("8.0", cell1.getValue());
    }

    @Test
    public void complexOperationUsingNamedRange2Test() {
        Cell cell1 = sheet.getCell("B4");
        cell1.setFormula("MAX(A1:A4) + MAX(range2)");

        assertEquals("14.0", cell1.getValue());
    }


}
