package ar.fiuba.tdd.tp1.newtests;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrintfTest {

    private Book book;
    private Sheet sheet;

    @Before
    public void setUp() {
        book = new Book();
        sheet = book.createNewSheet("hoja1", 100, 100);

        Cell cell1 = sheet.getCell("A1");
        cell1.setValue("Como $0");

        Cell cell2 = sheet.getCell("A2");
        cell2.setValue("estas");

        Cell cell3 = sheet.getCell("A3");
        cell3.setValue("corres");
    }

    @After
    public void tearDown() {
        sheet = null;
        book = null;
    }


    @Test
    public void basicPrintTest1() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("PRINTF(Hola$0 , A1)");

        assertEquals("Hola Como $0", cell.getValue());
    }

    @Test
    public void basicPrintTest2() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("PRINTF(Hola $0, comoestas)");

        assertEquals("Hola comoestas", cell.getValue());
    }

    @Test
    public void basicPrintTest3() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("PRINTF(Hola $0 $1 $2, Raul, como, estas)");

        assertEquals("Hola Raul como estas", cell.getValue());
    }

    @Test
    public void basicPrintTest4() {
        Cell cell = sheet.getCell("B1");
        cell.setFormula("PRINTF(Hola $0, Mundo)");

        assertEquals("Hola Mundo", cell.getValue());
    }
}
