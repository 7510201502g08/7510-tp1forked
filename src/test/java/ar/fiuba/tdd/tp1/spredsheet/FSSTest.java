package ar.fiuba.tdd.tp1.spredsheet;

import ar.fiuba.tdd.tp1.formatter.DateFormatter;
import ar.fiuba.tdd.tp1.formatter.Formatter;
import ar.fiuba.tdd.tp1.formatter.MoneyFormatter;
import ar.fiuba.tdd.tp1.formatter.NumberFormatter;
import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.CellsReferencesFinder;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.assertTrue;

public class FSSTest {

    @Test
    public void setAndGetCellValue() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("a");
        Cell cell2 = sheet.getCell(5, 5);
        assertTrue(cell2.getValue().equals("a"));
    }

    @Test
    public void doubleSetValueAndUndo() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("a");
        cell1.setValue("b");
        book.undo();
        Cell cell2 = sheet.getCell(5, 5);
        assertTrue(cell2.getValue().equals("a"));
    }

    @Test
    public void doubleSetValueAndUndoRedo() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("a");
        cell1.setValue("b");
        book.undo();
        book.redo();
        Cell cell2 = sheet.getCell(5, 5);
        assertTrue(cell2.getValue().equals("b"));
    }

    @Test
    public void resolveFormulaAddAndSub() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);

        //F6
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("10");

        //C3
        Cell cell2 = sheet.getCell(2, 2);
        cell2.setValue("15");

        //formula = 10+15-6+10 = 29
        Cell cell3 = sheet.getCell(1, 1);
        cell3.setFormula("F6+C3-6+F6");
        assertTrue(cell3.getValue().equals("29.0"));
    }

    @Test
    public void resolveFormulaAddAndSubWithParentesis() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);

        //F6
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("10");

        //C3
        Cell cell2 = sheet.getCell(2, 2);
        cell2.setValue("15");

        //formula = 10+15-6+10 = 29
        Cell cell3 = sheet.getCell(1, 1);
        cell3.setFormula("F6+C3-(6+F6)-((9-7)-1)");
        assertTrue(cell3.getValue().equals("8.0"));
    }

    @Test
    public void resolveFormulaAddAndSubAndMultWithParentesis() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);

        //F6
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("10");

        //C3
        Cell cell2 = sheet.getCell(2, 2);
        cell2.setValue("15");

        //formula = 10+15-6+10 = 29
        Cell cell3 = sheet.getCell(1, 1);
        cell3.setFormula("F6+C3-(6+F6)-((9-7)*2-1)");
        assertTrue(cell3.getValue().equals("6.0"));
    }

    @Test
    public void resolveFormulaAddUndo() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);

        //F6
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("10");

        //C3
        Cell cell2 = sheet.getCell(2, 2);
        cell2.setValue("15");

        //formula = 10+15-6+10 = 29
        Cell cell3 = sheet.getCell(1, 1);
        cell3.setFormula("F6+C3-6+F6");
        book.undo();
        assertTrue(cell3.getValue() == null);
    }

    @Test
    public void resolveFormulaAddUndoRedo() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);

        //F6
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("10");

        //C3
        Cell cell2 = sheet.getCell(2, 2);
        cell2.setValue("15");

        //formula B2: 10+15-6+10 = 29
        Cell cell3 = sheet.getCell(1, 1);
        cell3.setFormula("F6+C3-6+F6");
        book.undo();
        book.redo();
        assertTrue(cell3.getValue().equals("29.0"));
    }

    @Test
    public void overloadFormulas() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);

        //F6
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("10");

        //C3
        Cell cell2 = sheet.getCell(2, 2);
        cell2.setValue("15");

        //formula B2: 10+15-6+10 = 29 
        Cell cell3 = sheet.getCell(1, 1);
        cell3.setFormula("F6+C3-6+F6");

        Cell cell4 = sheet.getCell(1, 2);
        cell4.setFormula("B2+1");

        assertTrue(cell4.getValue().equals("30.0"));
    }

    @Test
    public void cloneCellValue() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);

        //F6
        Cell cell1 = sheet.getCell(5, 5);
        cell1.setValue("10");

        Cell cell2 = sheet.getCell(1, 2);
        cell2.setFormula("F6");

        book.undo();
        book.redo();
        assertTrue(cell2.getValue().equals("10.0"));
    }

    @Test
    public void sumCellWithoutValue() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);

        //F6
        Cell cell1 = sheet.getCell("F6");
        cell1.setValue("10");

        Cell cell2 = sheet.getCell("B1");
        cell2.setFormula("F6+A1");

        assertTrue(cell2.getValue().equals("Fórmula inválida"));
    }

    @Test
    public void sumUnexistentCell() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);

        //F6
        Cell cell1 = sheet.getCell("F6");
        cell1.setValue("10");

        Cell cell2 = sheet.getCell("B1");
        cell2.setFormula("F6+Z10");

        assertTrue(cell2.getValue().equals("Fórmula inválida"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setValueNullToACell() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 10, 10);
        Cell cell = sheet.getCell(5, 5);
        cell.setValue(null);
    }

    @Test
    public void getCalculateFormulaWithCellsFromDifferentSheets() {
        Book book = new Book();
        Sheet sheet1 = book.createNewSheet("hoja1", 10, 10);
        Sheet sheet2 = book.createNewSheet("hoja2", 10, 10);

        //F6 (sheet 1)
        Cell cell1 = sheet1.getCell(5, 5);
        cell1.setValue("10");

        //C3 (sheet 1)
        Cell cell2 = sheet1.getCell(2, 2);
        cell2.setValue("15");

        //A1 (sheet 2)
        Cell cell3 = sheet2.getCell(0, 0);
        cell3.setValue("15");

        //formula = 10+15-6+10 = 29
        Cell cell4 = sheet1.getCell(1, 1);
        cell4.setFormula("F6+C3-6+F6+hoja2!A1");
        assertTrue(cell4.getValue().equals("44.0"));
    }

    @Test
    public void invalidFormula() {
        Book book = new Book();
        Sheet sheet1 = book.createNewSheet("hoja1", 10, 10);

        Cell cell1 = sheet1.getCell(1, 1);
        cell1.setFormula("F6>invalido");
        assertTrue(cell1.getValue().equals("Fórmula inválida"));
    }

    @Test
    public void concatenation() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 100, 100);

        Cell cell1 = sheet.getCell("A1");
        cell1.setValue("ABC");

        Cell cell2 = sheet.getCell("A2");
        cell2.setValue("DEF");

        Cell cell3 = sheet.getCell("A3");
        cell3.setValue("GHI");

        Cell cell4 = sheet.getCell("A5");
        cell4.setValue("JKL");

        Cell cell5 = sheet.getCell("B1");
        cell5.setFormula("CONCAT(A1:A3,A5)");

        assertTrue(cell5.getValue().equals("ABCDEFGHIJKL"));
    }

    @Test
    public void invalidFormulaConcatenationAndArithmetic() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 100, 100);

        Cell cell1 = sheet.getCell("A1");
        cell1.setValue("ABC");

        Cell cell2 = sheet.getCell("A2");
        cell2.setValue("DEF");

        Cell cell3 = sheet.getCell("A3");
        cell3.setValue("GHI");

        Cell cell4 = sheet.getCell("A5");
        cell4.setValue("8");

        Cell cell5 = sheet.getCell("B1");
        cell5.setFormula("CONCAT(A1:A3,A1)+A5");

        assertTrue(cell5.getValue().equals("Fórmula inválida"));
    }

    @Test
    public void formulaAverageAndSum() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 100, 100);

        Cell cell1 = sheet.getCell("A1");
        cell1.setValue("6");

        Cell cell2 = sheet.getCell("A2");
        cell2.setValue("7");

        Cell cell3 = sheet.getCell("A3");
        cell3.setValue("8");

        Cell cell4 = sheet.getCell("A5");
        cell4.setValue("3");

        Cell cell5 = sheet.getCell("B1");
        cell5.setFormula("AVERAGE(A1:A3)+A5");

        assertTrue(cell5.getValue().equals("10.0"));
    }

    @Test
    public void formulaAgerageMaxAndMin() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 100, 100);

        Cell cell1 = sheet.getCell("A1");
        cell1.setValue("6");

        Cell cell2 = sheet.getCell("A2");
        cell2.setValue("7");

        Cell cell3 = sheet.getCell("A3");
        cell3.setValue("8");

        Cell cell4 = sheet.getCell("B1");
        cell4.setFormula("AVERAGE(A1:A3)+MIN(A1:A3)+MAX(A1:A3)");

        assertTrue(cell4.getValue().equals("21.0"));
    }

    @Test
    public void cellWithNumberFormatter() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 100, 100);
        Cell cell1 = sheet.getCell("A1");
        cell1.setFormatter(new NumberFormatter(3));
        cell1.setValue("16.4");
        assertTrue(cell1.getValue().equals("16.400"));
    }

    @Test
    public void cellWithDateFormatterYearMonthDay() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 100, 100);
        Cell cell1 = sheet.getCell("A1");
        cell1.setFormatter(new DateFormatter("yyyy-MM-dd"));
        cell1.setValue("2012-07-14T00:00:00Z");
        assertTrue(cell1.getValue().equals("2012-07-14"));
    }

    @Test
    public void cellWithDateFormatterDayMonthYear() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 100, 100);
        Cell cell1 = sheet.getCell("A1");
        cell1.setFormatter(new DateFormatter("dd-MM-yyyy"));
        cell1.setValue("2012-07-14T00:00:00Z");
        assertTrue(cell1.getValue().equals("14-07-2012"));
    }

    @Test
    public void cellWithDateFormatterYearDayMonth() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 100, 100);
        Cell cell1 = sheet.getCell("A1");
        cell1.setFormatter(new DateFormatter("MM-dd-yyyy"));
        cell1.setValue("2012-07-14T00:00:00Z");
        assertTrue(cell1.getValue().equals("07-14-2012"));
    }

    @Test
    public void cellWithMoneyFormatter() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 100, 100);
        Cell cell1 = sheet.getCell("A1");
        cell1.setFormatter(new MoneyFormatter(2, "ARS"));
        cell1.setValue("25.5");
        String val = cell1.getValue();
        assertTrue(val.equals("ARS 25.50"));
    }

    @Test
    public void getParametersMoneyFormatter() {
        Book book = new Book();
        Sheet sheet = book.createNewSheet("hoja1", 100, 100);
        Cell cell1 = sheet.getCell("A1");
        cell1.setFormatter(new MoneyFormatter(2, "ARS"));
        Formatter formatter = cell1.getFormatter();
        assertTrue(formatter.getName().equals("Money"));
        assertTrue(formatter.getParamters().containsKey("Money.Symbol"));
        assertTrue(formatter.getParamters().get("Money.Symbol").equals("ARS"));
        assertTrue(formatter.getParamters().containsKey("Number.Decimal"));
        assertTrue(formatter.getParamters().get("Number.Decimal").equals("2"));
    }

    @Test
    public void findReferences() {
        Book book = new Book();
        Sheet sheet1 = book.createNewSheet("hoja1", 100, 100);
        book.createNewSheet("hoja2", 100, 100);

        Cell cell1 = sheet1.getCell("A1");
        cell1.setFormula("hoja2!A6+A6+A3");

        CellsReferencesFinder finder = new CellsReferencesFinder(cell1);

        LinkedList<String> refs = finder.getCellsReferencesNames();
        assertTrue(refs.contains("A3") && refs.contains("A6") && refs.contains("hoja2!A6"));
    }

    @Test
    public void findReferencesCells() {
        Book book = new Book();
        Sheet sheet1 = book.createNewSheet("hoja1", 100, 100);
        Sheet sheet2 = book.createNewSheet("hoja2", 100, 100);

        Cell cell1 = sheet1.getCell("A1");
        cell1.setFormula("hoja2!A6+A6+A3");

        Cell cell2 = sheet1.getCell("A6");

        Cell cell3 = sheet1.getCell("A3");

        Cell cell4 = sheet2.getCell("A6");

        CellsReferencesFinder finder = new CellsReferencesFinder(cell1);

        LinkedList<Cell> refs = finder.getCellsReferences();
        assertTrue(refs.contains(cell2) && refs.contains(cell3) && refs.contains(cell4));
    }

    @Test
    public void cycleFormula() {
        Book book = new Book();
        Sheet sheet1 = book.createNewSheet("hoja1", 100, 100);
        Cell cell1 = sheet1.getCell("A1");
        cell1.setFormula("=A2");
        Cell cell2 = sheet1.getCell("A2");
        cell2.setFormula("=A3");
        Cell cell3 = sheet1.getCell("A3");
        cell3.setFormula("=A1");
        assertTrue(cell1.getValue().equals("Referencias circulares"));
    }

    @Test
    public void recalculate() {
        Book book = new Book();
        Sheet sheet1 = book.createNewSheet("hoja1", 10, 10);

        //F6 (sheet 1)
        Cell cell1 = sheet1.getCell(5, 5);
        cell1.setFormatter(new NumberFormatter(2));
        cell1.setValue("10");

        Cell cell4 = sheet1.getCell(1, 1);
        cell4.setFormatter(new NumberFormatter(2));
        cell4.setFormula("F6+30");

        cell1.setValue("20");

        String val = cell4.getValue();

        assertTrue(cell4.getValue().equals("50.00"));
    }


}