package ar.fiuba.tdd.tp1.spredsheet;

import ar.fiuba.tdd.tp1.acceptance.driver.*;
import ar.fiuba.tdd.tp1.formatter.DateFormatter;
import ar.fiuba.tdd.tp1.formatter.MoneyFormatter;
import ar.fiuba.tdd.tp1.formatter.NumberFormatter;
import ar.fiuba.tdd.tp1.formatter.StringFormatter;
import ar.fiuba.tdd.tp1.menu.*;
import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;

import java.util.*;

public class SpreadSheetFacade implements SpreadSheetTestDriver {

    HashMap<String, Book> books = new HashMap<String, Book>();
    Stack<String> undoBook = new Stack<>();
    Stack<String> redoBook = new Stack<>();
    ArrayList<String> stringCells = new ArrayList<String>();

    @Override
    public List<String> workBooksNames() {
        ArrayList<String> workBooksNames = new ArrayList<String>();
        Set<String> names = books.keySet();
        for (String name : names) {
            workBooksNames.add(name);
        }
        return workBooksNames;
    }

    @Override
    public void createNewWorkBookNamed(String name) {
        Book book = new Book();
        book.createNewSheet("default", 100, 100);
        books.put(name, book);
    }

    @Override
    public void createNewWorkSheetNamed(String workbookName, String name) {
        if (books.containsKey(workbookName)) {
            Book book = books.get(workbookName);
            book.createNewSheet(name, 100, 100);
            undoBook.push(workbookName);
        }
    }

    @Override
    public List<String> workSheetNamesFor(String workBookName) {
        ArrayList<String> sheetsNames = new ArrayList<String>();
        Collection<Sheet> sheets = books.get(workBookName).getAllSheets();
        for (Sheet sheet : sheets) {
            sheetsNames.add(sheet.getName());
        }
        return sheetsNames;
    }

    @Override
    public void setCellValue(String workBookName, String workSheetName, String cellId, String value) {
        Collection<Sheet> sheets = books.get(workBookName).getAllSheets();
        for (Sheet sheet : sheets) {
            value = value.replace("!" + sheet.getName() + ".", sheet.getName() + "!");
        }
        Book book = books.get(workBookName);
        if (value.contains("=")) {
            book.getSheet(workSheetName).getCell(cellId).setFormula(value.substring(1));
        } else {
            book.getSheet(workSheetName).getCell(cellId).setValue(value);
            book.getSheet(workSheetName).getCell(cellId).writeFormula("");
        }
        undoBook.push(workBookName);
    }

    @Override
    public String getCellValueAsString(String workBookName, String workSheetName, String cellId) {
        Book book = books.get(workBookName);
        try {
            Sheet sheet = book.getSheet(workSheetName);
            Cell cell = sheet.getCell(cellId);
            if (cell.getValue().equals("Fórmula inválida")) {
                return "Error:BAD_FORMULA";
            } else if (cell.getValue().equals("NOT MONEY")) {
                return "Error:BAD_CURRENCY";
            } else if (cell.getValue().equals("BAD DATE")) {
                return "Error:BAD_DATE";
            } else if (stringCells.contains(cellId) && cell.getFormula() != "") {
                return "=" + cell.getFormula();
            } else {
                return cell.getValue();
            }
        } catch (Exception ex) {
            throw new UndeclaredWorkSheetException();
        }
    }

    @Override
    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellId) {
        Book book = books.get(workBookName);
        Cell cell = book.getSheet(workSheetName).getCell(cellId);

        if (cell.getValue().equals("Fórmula inválida")) {
            throw new BadFormulaException();
        } else if (cell.getValue().equals("Referencias circulares")) {
            throw new BadReferenceException();
        } else if (stringCells.contains(cellId)) {
            throw new BadFormatException();
        } else {
            try {
                return Double.parseDouble(book.getSheet(workSheetName).getCell(cellId).getValue());
            } catch (Exception ex) {
                throw new BadFormulaException();
            }
        }
    }

    @Override
    public void undo() {
        if (!undoBook.empty()) {
            String bookToUndo = undoBook.pop();
            redoBook.push(bookToUndo);
            Book book = books.get(bookToUndo);
            book.undo();
        }
    }

    @Override
    public void redo() {
        if (!redoBook.empty()) {
            String bookToRedo = redoBook.pop();
            undoBook.push(bookToRedo);
            Book book = books.get(bookToRedo);
            book.redo();
        }
    }

    @Override
    public void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatter,
                                 String format) {
        Book book = books.get(workBookName);
        Cell cell = book.getSheet(workSheetName).getCell(cellId);
        if (formatter == "symbol") {
            MoneyFormatter moneyFormatter = (MoneyFormatter) cell.getFormatter();
            moneyFormatter.setSymbol(format);
        } else if (formatter == "decimal") {
            NumberFormatter moneyFormatter = (NumberFormatter) cell.getFormatter();
            moneyFormatter.setDecimal(Integer.parseInt(format));
        } else if (formatter == "format") {
            format = format.replace("YYYY-MM-DD", "yyyy-MM-dd");
            format = format.replace("MM-DD-YYYY", "MM-dd-yyyy");
            format = format.replace("DD-MM-YYYY", "dd-MM-yyyy");
            DateFormatter moneyFormatter = (DateFormatter) cell.getFormatter();
            moneyFormatter.setFormat(format);
        }
    }

    @Override
    public void setCellType(String workBookName, String workSheetName, String cellId, String type) {
        Book book = books.get(workBookName);
        Cell cell = book.getSheet(workSheetName).getCell(cellId);
        if (type == "Date") {
            stringCells.remove(cellId);
            cell.setFormatter(new DateFormatter("dd-MM-yyyy"));
        } else if (type == "Number") {
            stringCells.remove(cellId);
            cell.setFormatter(new NumberFormatter(0));
        } else if (type == "Currency") {
            stringCells.remove(cellId);
            cell.setFormatter(new MoneyFormatter(0, "$"));
        } else if (type == "String") {
            stringCells.add(cellId);
            cell.setFormatter(new StringFormatter());
        }
    }

    @Override
    public void persistWorkBook(String workBookName, String fileName) {
        Exporter exporter = new JsonExporter();
        Book book = books.get(workBookName);
        try {
            exporter.exportAFile(book, fileName, workBookName);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void reloadPersistedWorkBook(String fileName) {
        Importer importer = new JsonImporter();
        try {
            Book book = importer.importFile(fileName);
            books.remove(book.getName());
            books.put(book.getName(), book);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveAsCSV(String workBookName, String sheetName, String path) {
        Exporter exporte = new CSVExporter();
        try {
            Book book = new Book();
            Collection<Sheet> sheets = new LinkedList<Sheet>();
            sheets.add(books.get(workBookName).getSheet(sheetName));
            book.setSheets(sheets);
            exporte.exportAFile(book, workBookName, path);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void loadFromCSV(String workBookName, String sheetName, String path) {
        Importer importer = new CsvImporter();
        try {
            Book book = importer.importFile(path);
            // Siempre va a tener una sola sheet y le cambio el nombre
            for (Sheet sheet : book.getSheets()) {
                sheet.setName(sheetName);
            }
            book.setName(workBookName);
            books.remove(workBookName);
            books.put(workBookName, book);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int sheetCountFor(String workBookName) {
        return books.get(workBookName).getSheets().size();
    }
}
